<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Odbornost;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class OdbornostAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->add('nazev')
            ->add('popis');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper
            ->add('nazev')
            ->add('popis');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
            ->add('nazev')
            ->add('popis');
    }

//    public function toString($object)
//    {
//        return $object instanceof Mesto
//            ? $object->getJmeno()
//            : 'Město'; // shown in the breadcrumb on the create view
//    }
}
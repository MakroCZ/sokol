<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Cvicitel;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CvicitelAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper->add('idCvicitelskePrukazky')
                   ->add('druhyEmail')
                   ->add('osoba', 'sonata_type_model_list', array(
                       'class' => 'AppBundle\Entity\Osoba',
                   ), array(
                       'admin_code' => 'admin.osoba'
                   ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper
            ->add('osoba');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
            ->add('osoba', 'text', array(
                'admin_code' => 'admin.osoba'
            ))
            ->add('druhyEmail');
    }

}
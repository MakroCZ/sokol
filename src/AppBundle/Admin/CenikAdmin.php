<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Cenik;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CenikAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nazevZnamky')
            ->add('cena');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nazevZnamky')
            ->add('cena');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nazevZnamky')
            ->add('cena');

    }
}

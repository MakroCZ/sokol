<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Skupina;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SkupinaAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->with('Skupina')
                ->add('nazev')
                ->add('skupinaTyp', 'sonata_type_model_list', array(
                                'class' => 'AppBundle\Entity\SkupinaTyp',
                                ))
            ->end()
            ->with('Podrobnosti')
                ->add('skupinaPodrobnosti', 'sonata_type_admin')
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper
            ->add('nazev')
            ->add('skupinaTyp');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
            ->add('nazev')
            ->add('skupinaTyp', 'text')
        ;
    }
}

<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Platba;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PlatbaAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->add('osoba', 'sonata_type_model_list', array(
                'class' => 'AppBundle\Entity\Osoba',
            ), array(
                'admin_code' => 'admin.osoba'
            ))
            ->add('cena', 'sonata_type_model_list', array(
                'class' => 'AppBundle\Entity\Cenik',
            ))
            ->add('stavPlatby', 'sonata_type_model_list', array(
                'class' => 'AppBundle\Entity\StavPlatby',
            ))
            ->add('datum', 'sonata_type_date_picker', array(
                'format' => 'dd.MM.yyyy'
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper
            ->add('osoba')
            ->add('cena')
            ->add('stavPlatby');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
            ->add('osoba', 'text', array(
                'admin_code' => 'admin.osoba'
            ))
            ->add('cena', 'text')
            ->add('stavPlatby', 'text')
            ->add('datum')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                )
            ));
    }


}
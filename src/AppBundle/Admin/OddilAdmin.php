<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OddilAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->add('nazev', 'text')
            ->add('kartoteka', 'sonata_type_model_list', array(
                'class' => 'AppBundle\Entity\Skupina',
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper
            ->add('kartoteka.nazev');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
            ->add('nazev')

            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'show' => array(),
                )
            ));
    }

    protected function configureShowFields(ShowMapper $showMapper) {

        $showMapper
            ->add('cvicitel')
            ->add('kartoteka.osobaSkupina');
    }

}

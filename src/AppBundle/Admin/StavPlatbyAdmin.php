<?php

namespace AppBundle\Admin;

use AppBundle\Entity\StavPlatby;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class StavPlatbyAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper->add('stavPlatby');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper->add('stavPlatby');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper->add('stavPlatby');
    }

//    public function toString($object)
//    {
//        return $object instanceof Mesto
//            ? $object->getJmeno()
//            : 'Město'; // shown in the breadcrumb on the create view
//    }
}
<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Mesto;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MestoAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper->add('jmeno');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper->add('jmeno');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper->add('jmeno');
    }


}
<?php

namespace AppBundle\Admin;

use AppBundle\DBAL\Types\PohlaviType;
use AppBundle\Entity\Skupina;
use AppBundle\Services\KartotekaServices;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class OsobaAdmin extends Admin
{

    /**
     * @var KartotekaServices
     */
    protected $kartotekaServices;

    protected function configureFormFields(FormMapper $formMapper)
    {


        $formMapper
            ->with('Osobní údaje', array('class' => 'col-md-6'))
                ->add('idPrukazky')
                ->add('jmeno')
                ->add('prijmeni')
                ->add('titulPred')
                ->add('titulZa')
                ->add('pohlavi', 'choice', array(
                    'choices' => PohlaviType::getChoices()
                ))
                ->add('datumNarozeni', 'sonata_type_date_picker', array(
                    'format' => 'dd.MM.yyyy'
                ))
                ->add('rodneCislo')
                ->add('bydliste')
                ->add('mesto', 'sonata_type_model_list', array(
                    'class' => 'AppBundle\Entity\Mesto',
                    ))

                ->add('psc')
                ->add('email')
                ->add('telefon')
                ->add('telefonNaRodice')
            ->end()
            ->with('Další údaje', array('class' => 'col-md-6'))
                ->add('podpisZdravotniZpusobilosti')
                ->add('souhlasSFocenim')
                ->add('zdravotniPotize')
                ->add('registrovanySportovec')
                ->add('funkcionar')
                ->add('technickoOrganizacniPracovnik')
            ->end()
            ->add('skupiny', 'entity', array(
                'class' => 'AppBundle\Entity\Skupina',
                'choices' => $this->kartotekaServices->getKartoteky(),
                'multiple' => true
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

        $datagridMapper
            ->add('idPrukazky')
            ->add('jmeno')
            ->add('prijmeni')
            ->add('datumNarozeni')
            ->add('pohlavi')
            ->add('podpisZdravotniZpusobilosti')
            ->add('mesto.jmeno')
            ->add('clenemOd')
            ->add('souhlasSFocenim');
    }

    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->add('idPrukazky')
            ->add('jmeno')
            ->add('prijmeni')
            ->add('datumNarozeni')
            ->add('email')
            ->add('bydliste')
            ->add('telefon')
            ->add('mesto.jmeno')
            ->add('psc')
            ->add('podpisZdravotniZpusobilosti')
            ->add('souhlasSFocenim')
            ->add('zdravotniPotize')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                )
            ));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {

        $showMapper
            ->with('Další údaje')
            ->add('titulPred')
            ->add('titulZa');
        if($this->isGranted('ROLE_SPRAVCE_KARTOTEKY'))
        {
            $showMapper->add('rodneCislo');
        }
        $showMapper
            ->add('telefonNaRodice')
            ->add('pohlavi')
            ->end()
            ->add('skupiny')
            ->add('platby')
            ->add('registrovanySportovec')
            ->add('funkcionar')
            ->add('technickoOrganizacniPracovnik');
    }

    public function setKartotekaService(KartotekaServices $kartotekaServices)
    {

        $this->kartotekaServices = $kartotekaServices;
    }


}

<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Kvalifikace;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class KvalifikaceAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper->add('kvalifikace');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper->add('kvalifikace');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper->add('kvalifikace');
    }

//    public function toString($object)
//    {
//        return $object instanceof Mesto
//            ? $object->getJmeno()
//            : 'Město'; // shown in the breadcrumb on the create view
//    }
}
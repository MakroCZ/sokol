<?php

namespace AppBundle\Admin;

use AppBundle\Entity\SkupinaPodrobnosti;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SkupinaPodrobnostiAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->add('ico')
            ->add('email')
            ->add('ulice')
            ->add('mesto', 'sonata_type_model_list', array(
                            'class' => 'AppBundle\Entity\Mesto',
                            ))
            ->add('psc');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper
            ->add('ico')
            ->add('ulice')
            ->add('psc')
            ->add('email');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
            ->add('ico')
            ->add('ulice')
            ->add('psc')
            ->add('email');
    }
}
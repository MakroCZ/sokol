<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Zkouska;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ZkouskaAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->add('cvicitel', 'sonata_type_model_list', array(
                'class' => 'AppBundle\Entity\Cvicitel'
            ))
            ->add('odbornost', 'sonata_type_model_list', array(
                'class' => 'AppBundle\Entity\Odbornost',
            ))
            ->add('kvalifikace', 'sonata_type_model_list', array(
                'class' => 'AppBundle\Entity\Kvalifikace'
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper
            ->add('cvicitel')
            ->add('odbornost')
            ->add('kvalifikace');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
            ->add('cvicitel', 'text')
            ->add('odbornost', 'text')
            ->add('kvalifikace', 'text');
    }

}
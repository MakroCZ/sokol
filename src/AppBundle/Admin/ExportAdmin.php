<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ExportAdmin extends Admin {

    protected $baseRouteName = 'export';
    protected $baseRoutePattern = 'export';

    protected function configureFormFields(FormMapper $formMapper) {

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper
            ->add('prijmeni')
            ->add('jmeno')
            ->add('rodneCislo')
            ->add('rok')
            ->add('mesic')
            ->add('den')
            ->add('pohlavi')
            ->add('bydliste')
            ->add('telefon')
            ->add('email')
            ->add('registrovanySportovec')
            ->add('jeTrener', 'boolean')
            ->add('jeRozhodci', 'boolean')
            ->add('technickoOrganizacniPracovnik')
            ->add('funkcionar')
            ->add('jeOstatni', 'boolean')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list'));
    }

}
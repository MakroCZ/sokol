<?php

namespace AppBundle\Admin;

use AppBundle\Entity\SkupinaTyp;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SkupinaTypAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper->add('id');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper) {

        $listMapper->add('id');
    }

    public function toString($object)
    {
        return $object instanceof SkupinaTyp
            ? $object->getId()
            : 'Typ skupiny'; // shown in the breadcrumb on the create view
    }
}
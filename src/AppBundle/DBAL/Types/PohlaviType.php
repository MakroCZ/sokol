<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class PohlaviType extends AbstractEnumType
{
    const MUZ    = 'Muž';
    const ZENA = 'Žena';

    protected static $choices = [
        self::MUZ    => 'Muž',
        self::ZENA   => 'Žena'
    ];
}
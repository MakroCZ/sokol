<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MistoHodinOddilu
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class MistoHodinOddilu extends Base
{
    /**
     * @ORM\Column(type="string")
     */
    protected $nazev;

    /**
     * @ORM\Column(type="string")
     */
    protected $adresa;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $lat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $lng;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\HodinaOddilu", mappedBy="misto")
     */
    protected $hodiny;

    /**
     * @return mixed
     */
    public function getNazev()
    {
        return $this->nazev;
    }

    /**
     * @param mixed $nazev
     */
    public function setNazev($nazev)
    {
        $this->nazev = $nazev;
    }

    /**
     * @return mixed
     */
    public function getAdresa()
    {
        return $this->adresa;
    }

    /**
     * @param mixed $adresa
     */
    public function setAdresa($adresa)
    {
        $this->adresa = $adresa;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param mixed $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return mixed
     */
    public function getHodiny()
    {
        return $this->hodiny;
    }

    /**
     * @param mixed $hodiny
     */
    public function setHodiny($hodiny)
    {
        $this->hodiny = $hodiny;
    }

}

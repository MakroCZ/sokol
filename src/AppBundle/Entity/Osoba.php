<?php

namespace AppBundle\Entity;

use AppBundle\DBAL\Types\PohlaviType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Class Osoba
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Osoba extends Base
{

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    protected $idPrukazky;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string")
     */
    protected $jmeno;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string")
     */
    protected $prijmeni;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="date")
     */
    protected $datumNarozeni;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", unique=true)
     */
    protected $rodneCislo;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string")
     */
    protected $bydliste;

    /**
     * @ORM\Column(type="string")
     */
    protected $telefon;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $telefonNaRodice;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $podpisZdravotniZpusobilosti = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titulPred;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titulZa;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Mesto", inversedBy="osoby")
     */
    protected $mesto;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $psc;

    /**
     * @ORM\Column(type="date")
     */
    protected $clenemOd;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $souhlasSFocenim;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Platba", mappedBy="osoba")
     */
    protected $platby;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Skupina", inversedBy="osoby", cascade={"persist"})
     */
    protected $skupiny;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $zdravotniPotize;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $registrovanySportovec;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $funkcionar;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $technickoOrganizacniPracovnik;

    /**
     * @ORM\Column(type="PohlaviType")
     * @Assert\NotBlank()
     */
    protected $pohlavi;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", mappedBy="osoba")
     */
    protected $user;

    function __construct()
    {
        $this->clenemOd = new \DateTime();
    }
    
    /**
     * @return mixed
     */
    public function getIdPrukazky()
    {
        return $this->idPrukazky;
    }

    /**
     * @param mixed $idPrukazky
     */
    public function setIdPrukazky($idPrukazky)
    {
        $this->idPrukazky = $idPrukazky;
    }

    /**
     * @return mixed
     */
    public function getJmeno()
    {
        return $this->jmeno;
    }

    /**
     * @param mixed $jmeno
     */
    public function setJmeno($jmeno)
    {
        $this->jmeno = $jmeno;
    }

    /**
     * @return mixed
     */
    public function getPrijmeni()
    {
        return $this->prijmeni;
    }

    /**
     * @param mixed $prijmeni
     */
    public function setPrijmeni($prijmeni)
    {
        $this->prijmeni = $prijmeni;
    }

    /**
     * @return mixed
     */
    public function getDatumNarozeni()
    {
        return $this->datumNarozeni;
    }

    /**
     * @param mixed $datumNarozeni
     */
    public function setDatumNarozeni($datumNarozeni)
    {
        $this->datumNarozeni = $datumNarozeni;
    }

    /**
     * @return mixed
     */
    public function getRodneCislo()
    {
        return $this->rodneCislo;
    }

    /**
     * @param mixed $rodneCislo
     */
    public function setRodneCislo($rodneCislo)
    {
        $this->rodneCislo = $rodneCislo;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getBydliste()
    {
        return $this->bydliste;
    }

    /**
     * @param mixed $bydliste
     */
    public function setBydliste($bydliste)
    {
        $this->bydliste = $bydliste;
    }

    /**
     * @return mixed
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * @param mixed $telefon
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    }

    /**
     * @return mixed
     */
    public function getTelefonNaRodice()
    {
        return $this->telefonNaRodice;
    }

    /**
     * @param mixed $telefonNaRodice
     */
    public function setTelefonNaRodice($telefonNaRodice)
    {
        $this->telefonNaRodice = $telefonNaRodice;
    }

    /**
     * @return mixed
     */
    public function getPodpisZdravotniZpusobilosti()
    {
        return $this->podpisZdravotniZpusobilosti;
    }

    /**
     * @param mixed $podpisZdravotniZpusobilosti
     */
    public function setPodpisZdravotniZpusobilosti($podpisZdravotniZpusobilosti)
    {
        $this->podpisZdravotniZpusobilosti = $podpisZdravotniZpusobilosti;
    }

    /**
     * @return mixed
     */
    public function getTitulPred()
    {
        return $this->titulPred;
    }

    /**
     * @param mixed $titulPred
     */
    public function setTitulPred($titulPred)
    {
        $this->titulPred = $titulPred;
    }

    /**
     * @return mixed
     */
    public function getTitulZa()
    {
        return $this->titulZa;
    }

    /**
     * @param mixed $titulZa
     */
    public function setTitulZa($titulZa)
    {
        $this->titulZa = $titulZa;
    }

    /**
     * @return mixed
     */
    public function getMesto()
    {
        return $this->mesto;
    }

    /**
     * @param mixed $mesto
     */
    public function setMesto($mesto)
    {
        $this->mesto = $mesto;
    }

    /**
     * @return mixed
     */
    public function getPsc()
    {
        return $this->psc;
    }

    /**
     * @param mixed $psc
     */
    public function setPsc($psc)
    {
        $this->psc = $psc;
    }

    /**
     * @return mixed
     */
    public function getClenemOd()
    {
        return $this->clenemOd;
    }

    /**
     * @param mixed $clenemOd
     */
    public function setClenemOd($clenemOd)
    {
        $this->clenemOd = $clenemOd;
    }

    /**
     * @return mixed
     */
    public function getSouhlasSFocenim()
    {
        return $this->souhlasSFocenim;
    }

    /**
     * @param mixed $souhlasSFocenim
     */
    public function setSouhlasSFocenim($souhlasSFocenim)
    {
        $this->souhlasSFocenim = $souhlasSFocenim;
    }

    /**
     * @return mixed
     */
    public function getPlatby()
    {
        return $this->platby;
    }

    /**
     * @param mixed $platby
     */
    public function setPlatby($platby)
    {
        $this->platby = $platby;
    }

    /**
     * @return mixed
     */
    public function getSkupiny()
    {
        return $this->skupiny;
    }

    /**
     * @param mixed $skupiny
     */
    public function setSkupiny($skupiny)
    {
        $this->skupiny = $skupiny;
    }
    
    /**
     * @return mixed
     */
    public function getZdravotniPotize()
    {
        return $this->zdravotniPotize;
    }

    /**
     * @param mixed $zdravotniPotize
     */
    public function setZdravotniPotize($zdravotniPotize)
    {
        $this->zdravotniPotize = $zdravotniPotize;
    }

    /**
     * @return mixed
     */
    public function getRegistrovanySportovec()
    {
        return $this->registrovanySportovec;
    }

    /**
     * @param mixed $registrovanySportovec
     */
    public function setRegistrovanySportovec($registrovanySportovec)
    {
        $this->registrovanySportovec = $registrovanySportovec;
    }

    /**
     * @return mixed
     */
    public function getFunkcionar()
    {
        return $this->funkcionar;
    }

    /**
     * @param mixed $funkcionar
     */
    public function setFunkcionar($funkcionar)
    {
        $this->funkcionar = $funkcionar;
    }

    /**
     * @return mixed
     */
    public function getTechnickoOrganizacniPracovnik()
    {
        return $this->technickoOrganizacniPracovnik;
    }

    /**
     * @param mixed $technickoOrganizacniPracovnik
     */
    public function setTechnickoOrganizacniPracovnik($technickoOrganizacniPracovnik)
    {
        $this->technickoOrganizacniPracovnik = $technickoOrganizacniPracovnik;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPohlavi()
    {
        return $this->pohlavi;
    }

    /**
     * @param mixed $pohlavi
     */
    public function setPohlavi($pohlavi)
    {
        $this->pohlavi = $pohlavi;
    }

    function __toString()
    {
        return $this->getPrijmeni() . " (" . $this->getIdPrukazky() . ")";
    }

    public function jeTrener()
    {

    }

    public function jeRozhodci()
    {

    }

    public function jeOstatni()
    {
        if($this->getTechnickoOrganizacniPracovnik()||$this->getFunkcionar()||$this->getRegistrovanySportovec()||$this->jeTrener()||$this->jeRozhodci()) {
            return false;
        } else {
            return true;
        }
    }

    public function getRok()
    {
        return $this->getDatumNarozeni()->format('Y');
    }

    public function getMesic()
    {
        return $this->getDatumNarozeni()->format('m');
    }

    public function getDen()
    {
        return $this->getDatumNarozeni()->format('d');
    }
}

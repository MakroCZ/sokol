<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Cenik
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Cenik extends Base
{
    /**
     * @ORM\Column(type="string")
     */
    protected $nazevZnamky;

    /**
     * @ORM\Column(type="integer")
     */
    protected $cena;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Platba", mappedBy="cena")
     */
    protected $platba;

    /**
     * @return mixed
     */
    public function getNazevZnamky()
    {
        return $this->nazevZnamky;
    }

    /**
     * @param mixed $nazevZnamky
     */
    public function setNazevZnamky($nazevZnamky)
    {
        $this->nazevZnamky = $nazevZnamky;
    }

    /**
     * @return mixed
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * @param mixed $cena
     */
    public function setCena($cena)
    {
        $this->cena = $cena;
    }

    /**
     * @return mixed
     */
    public function getPlatba()
    {
        return $this->platba;
    }

    /**
     * @param mixed $platba
     */
    public function setPlatba($platba)
    {
        $this->platba = $platba;
    }

    function __toString()
    {
        return $this->getNazevZnamky() . " (" . $this->getCena() . ")";
    }
}

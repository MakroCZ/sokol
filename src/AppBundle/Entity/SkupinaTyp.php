<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * Class SkupinaTyp
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class SkupinaTyp extends Base {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Skupina", mappedBy="skupinaTyp")
     */
    protected $skupiny;

    /**
     * @return mixed
     */
    public function getSkupiny()
    {
        return $this->skupiny;
    }

    /**
     * @param mixed $skupiny
     */
    public function setSkupiny($skupiny)
    {
        $this->skupiny = $skupiny;
    }


    function __toString()
    {
        return (string)$this->getId();
    }
}

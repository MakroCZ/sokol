<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Zkouska
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Zkouska extends Base
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Odbornost", inversedBy="zkousky")
     */
    protected $odbornost;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kvalifikace")
     */
    protected $kvalifikace;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cvicitel", inversedBy="zkousky")
     */
    protected $cvicitel;

    /**
     * @return mixed
     */
    public function getOdbornost()
    {
        return $this->odbornost;
    }

    /**
     * @param mixed $odbornost
     */
    public function setOdbornost($odbornost)
    {
        $this->odbornost = $odbornost;
    }

    /**
     * @return mixed
     */
    public function getKvalifikace()
    {
        return $this->kvalifikace;
    }

    /**
     * @param mixed $kvalifikace
     */
    public function setKvalifikace($kvalifikace)
    {
        $this->kvalifikace = $kvalifikace;
    }

    /**
     * @return mixed
     */
    public function getCvicitel()
    {
        return $this->cvicitel;
    }

    /**
     * @param mixed $cvicitel
     */
    public function setCvicitel($cvicitel)
    {
        $this->cvicitel = $cvicitel;
    }

}

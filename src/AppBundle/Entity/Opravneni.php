<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Opravneni
 * @package AppBundle\Entity
 */
class Opravneni extends Base
{
    /**
     * @ORM\Column(type="string")
     */
    protected $pravo;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OsobaSkupina", mappedBy="opravneni")
     */
    protected $osobaSkupina;

    /**
     * @return mixed
     */
    public function getPravo()
    {
        return $this->pravo;
    }

    /**
     * @param mixed $pravo
     */
    public function setPravo($pravo)
    {
        $this->pravo = $pravo;
    }

    /**
     * @return mixed
     */
    public function getOsobaSkupina()
    {
        return $this->osobaSkupina;
    }

    /**
     * @param mixed $osobaSkupina
     */
    public function setOsobaSkupina($osobaSkupina)
    {
        $this->osobaSkupina = $osobaSkupina;
    }

    function __toString()
    {
        return (string)$this->getPravo();
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class cvicitel
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Cvicitel extends Base
{
    /**
     * @ORM\Column(type="integer")
     */
    protected $idCvicitelskePrukazky;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $druhyEmail;

    /**
     * @var Osoba
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Osoba")
     */
    protected $osoba;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Zkouska", mappedBy="cvicitel")
     */
    protected $zkousky;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Oddil", inversedBy="cvicitele")
     */
    protected $oddily;

    /**
     * @return mixed
     */
    public function getIdCvicitelskePrukazky()
    {
        return $this->idCvicitelskePrukazky;
    }

    /**
     * @param mixed $idCvicitelskePrukazky
     */
    public function setIdCvicitelskePrukazky($idCvicitelskePrukazky)
    {
        $this->idCvicitelskePrukazky = $idCvicitelskePrukazky;
    }

    /**
     * @return mixed
     */
    public function getDruhyEmail()
    {
        return $this->druhyEmail;
    }

    /**
     * @param mixed $druhyEmail
     */
    public function setDruhyEmail($druhyEmail)
    {
        $this->druhyEmail = $druhyEmail;
    }

    /**
     * @return mixed
     */
    public function getOsoba()
    {
        return $this->osoba;
    }

    /**
     * @param mixed $osoba
     */
    public function setOsoba($osoba)
    {
        $this->osoba = $osoba;
    }

    /**
     * @return mixed
     */
    public function getZkousky()
    {
        return $this->zkousky;
    }

    /**
     * @param mixed $zkousky
     */
    public function setZkousky($zkousky)
    {
        $this->zkousky = $zkousky;
    }

    /**
     * @return mixed
     */
    public function getOddily()
    {
        return $this->oddily;
    }

    /**
     * @param mixed $oddily
     */
    public function setOddily($oddily)
    {
        $this->oddily = $oddily;
    }

    function __toString()
    {
        return (string)$this->getOsoba();
    }

}

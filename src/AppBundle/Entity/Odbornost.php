<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Odbornost
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Odbornost extends Base
{
    /**
     * @ORM\Column(type="string")
     */
    protected $nazev;

    /**
     * @ORM\Column(type="string")
     */
    protected $popis;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Zkouska", mappedBy="odbornost")
     */
    protected $zkousky;

    /**
     * @return mixed
     */
    public function getNazev()
    {
        return $this->nazev;
    }

    /**
     * @param mixed $nazev
     */
    public function setNazev($nazev)
    {
        $this->nazev = $nazev;
    }

    /**
     * @return mixed
     */
    public function getPopis()
    {
        return $this->popis;
    }

    /**
     * @param mixed $popis
     */
    public function setPopis($popis)
    {
        $this->popis = $popis;
    }

    /**
     * @return mixed
     */
    public function getZkousky()
    {
        return $this->zkousky;
    }

    /**
     * @param mixed $zkousky
     */
    public function setZkousky($zkousky)
    {
        $this->zkousky = $zkousky;
    }

    function __toString()
    {
        return (string)$this->getNazev();
    }
}

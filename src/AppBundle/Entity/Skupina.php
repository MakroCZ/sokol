<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Skupina
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Skupina extends Base
{
    /**
     * @ORM\Column(type="string")
     */
    protected $nazev;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\SkupinaPodrobnosti", inversedBy="skupina", cascade={"persist"})
     */
    protected $skupinaPodrobnosti;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Oddil", mappedBy="kartoteka")
     */
    protected $oddily;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Skupina", mappedBy="nadrazenaSkupina")
     */
    protected $podrazeneSkupiny;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Osoba", mappedBy="skupiny", cascade={"persist"})
     */
    protected $osoby;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Skupina", inversedBy="podrazeneSkupiny")
     */
    protected $nadrazenaSkupina;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SkupinaTyp", inversedBy="skupiny", cascade={"persist"})
     */
    protected $skupinaTyp;

    /**
     * @return mixed
     */
    public function getNazev()
    {
        return $this->nazev;
    }

    /**
     * @param mixed $nazev
     */
    public function setNazev($nazev)
    {
        $this->nazev = $nazev;
    }

    /**
     * @return mixed
     */
    public function getSkupinaPodrobnosti()
    {
        return $this->skupinaPodrobnosti;
    }

    /**
     * @param mixed $skupinaPodrobnosti
     */
    public function setSkupinaPodrobnosti($skupinaPodrobnosti)
    {
        $this->skupinaPodrobnosti = $skupinaPodrobnosti;
    }

    /**
     * @return mixed
     */
    public function getOddily()
    {
        return $this->oddily;
    }

    /**
     * @param mixed $oddily
     */
    public function setOddily($oddily)
    {
        $this->oddily = $oddily;
    }

    /**
     * @return mixed
     */
    public function getPodrazeneSkupiny()
    {
        return $this->podrazeneSkupiny;
    }

    /**
     * @param mixed $podrazeneSkupiny
     */
    public function setPodrazeneSkupiny($podrazeneSkupiny)
    {
        $this->podrazeneSkupiny = $podrazeneSkupiny;
    }

    /**
     * @return mixed
     */
    public function getOsoby()
    {
        return $this->osoby;
    }

    /**
     * @param mixed $osoby
     */
    public function setOsoby($osoby)
    {
        $this->osoby = $osoby;
    }
    
    /**
     * @return mixed
     */
    public function getNadrazenaSkupina()
    {
        return $this->nadrazenaSkupina;
    }

    /**
     * @param mixed $nadrazenaSkupina
     */
    public function setNadrazenaSkupina($nadrazenaSkupina)
    {
        $this->nadrazenaSkupina = $nadrazenaSkupina;
    }

    /**
     * @return mixed
     */
    public function getSkupinaTyp()
    {
        return $this->skupinaTyp;
    }

    /**
     * @param mixed $skupinaTyp
     */
    public function setSkupinaTyp($skupinaTyp)
    {
        $this->skupinaTyp = $skupinaTyp;
    }

    function __toString()
    {
        return (string)$this->getNazev();
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Platba
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Platba extends Base
{
    /**
     * @ORM\Column(type="date")
     */
    protected $datum;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cenik", inversedBy="platba")
     */
    protected $cena;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StavPlatby")
     */
    protected $stavPlatby;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Osoba", inversedBy="platby")
     */
    protected $osoba;

    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param mixed $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }

    /**
     * @return mixed
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * @param mixed $cena
     */
    public function setCena($cena)
    {
        $this->cena = $cena;
    }

    /**
     * @return mixed
     */
    public function getStavPlatby()
    {
        return $this->stavPlatby;
    }

    /**
     * @param mixed $stavPlatby
     */
    public function setStavPlatby($stavPlatby)
    {
        $this->stavPlatby = $stavPlatby;
    }

    /**
     * @return mixed
     */
    public function getOsoba()
    {
        return $this->osoba;
    }

    /**
     * @param mixed $osoba
     */
    public function setOsoba($osoba)
    {
        $this->osoba = $osoba;
    }

    function __toString()
    {
        return $this->getCena().'';
    }

}

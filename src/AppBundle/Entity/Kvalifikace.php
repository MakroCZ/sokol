<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Kvalifikace
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Kvalifikace extends Base
{
    /**
     * @ORM\Column(type="string")
     */
    protected $kvalifikace;

    /**
     * @return mixed
     */
    public function getKvalifikace()
    {
        return $this->kvalifikace;
    }

    /**
     * @param mixed $kvalifikace
     */
    public function setKvalifikace($kvalifikace)
    {
        $this->kvalifikace = $kvalifikace;
    }

    function __toString()
    {
        return (string)$this->getKvalifikace();
    }
}

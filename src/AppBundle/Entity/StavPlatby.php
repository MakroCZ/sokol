<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class StavPlatby
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class StavPlatby extends Base
{

    /**
     * @ORM\Column(type="string")
     */
    protected $stavPlatby;

    /**
     * @return mixed
     */
    public function getStavPlatby()
    {
        return $this->stavPlatby;
    }

    /**
     * @param mixed $stavPlatby
     */
    public function setStavPlatby($stavPlatby)
    {
        $this->stavPlatby = $stavPlatby;
    }

    function __toString()
    {
        return (string)$this->getStavPlatby();
    }

}

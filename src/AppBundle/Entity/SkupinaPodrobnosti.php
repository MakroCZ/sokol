<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class SkupinaPodrobnosti
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class SkupinaPodrobnosti extends Base
{
    /**
     * @ORM\Column(type="string")
     */
    protected $ico;

    /**
     * @ORM\Column(type="string")
     */
    protected $ulice;

    /**
     * @ORM\Column(type="integer")
     */
    protected $psc;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Skupina", mappedBy="skupinaPodrobnosti")
     */
    protected $skupina;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Mesto", inversedBy="skupinaPodrobnosti")
     */
    protected $mesto;

    /**
     * @return mixed
     */
    public function getIco()
    {
        return $this->ico;
    }

    /**
     * @param mixed $ico
     */
    public function setIco($ico)
    {
        $this->ico = $ico;
    }

    /**
     * @return mixed
     */
    public function getUlice()
    {
        return $this->ulice;
    }

    /**
     * @param mixed $ulice
     */
    public function setUlice($ulice)
    {
        $this->ulice = $ulice;
    }

    /**
     * @return mixed
     */
    public function getPsc()
    {
        return $this->psc;
    }

    /**
     * @param mixed $psc
     */
    public function setPsc($psc)
    {
        $this->psc = $psc;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSkupina()
    {
        return $this->skupina;
    }

    /**
     * @param mixed $skupina
     */
    public function setSkupina($skupina)
    {
        $this->skupina = $skupina;
    }

    /**
     * @return mixed
     */
    public function getMesto()
    {
        return $this->mesto;
    }

    /**
     * @param mixed $mesto
     */
    public function setMesto($mesto)
    {
        $this->mesto = $mesto;
    }

    function __toString()
    {
        return (string)$this->getSkupina();
    }
}

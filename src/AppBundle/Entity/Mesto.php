<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Mesto
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class Mesto extends Base
{
    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string")
     */
    protected $jmeno;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Osoba", mappedBy="mesto")
     */
    protected $osoby;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SkupinaPodrobnosti", mappedBy="mesto")
     */
    protected $skupinaPodrobnosti;

    /**
     * @return mixed
     */
    public function getJmeno()
    {
        return $this->jmeno;
    }

    /**
     * @param mixed $jmeno
     */
    public function setJmeno($jmeno)
    {
        $this->jmeno = $jmeno;
    }

    /**
     * @return mixed
     */
    public function getOsoby()
    {
        return $this->osoby;
    }

    /**
     * @param mixed $osoby
     */
    public function setOsoby($osoby)
    {
        $this->osoby = $osoby;
    }

    /**
     * @return mixed
     */
    public function getSkupinaPodrobnosti()
    {
        return $this->skupinaPodrobnosti;
    }

    /**
     * @param mixed $skupinaPodrobnosti
     */
    public function setSkupinaPodrobnosti($skupinaPodrobnosti)
    {
        $this->skupinaPodrobnosti = $skupinaPodrobnosti;
    }

    function __toString()
    {
        return (string)$this->getJmeno();
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class HodinaOddilu
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class HodinaOddilu extends Base
{
    /**
     * @ORM\Column(type="string")
     */
    protected $datum;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MistoHodinOddilu", inversedBy="hodiny")
     */
    protected $misto;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Oddil", inversedBy="hodiny")
     */
    protected $oddil;

    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param mixed $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }

    /**
     * @return mixed
     */
    public function getMisto()
    {
        return $this->misto;
    }

    /**
     * @param mixed $misto
     */
    public function setMisto($misto)
    {
        $this->misto = $misto;
    }

    /**
     * @return mixed
     */
    public function getOddil()
    {
        return $this->oddil;
    }

    /**
     * @param mixed $oddil
     */
    public function setOddil($oddil)
    {
        $this->oddil = $oddil;
    }

}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Oddil
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Oddil extends Base
{
    /**
     * @var string
     */
    protected $nazev;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\HodinaOddilu", mappedBy="oddil")
     */
    protected $hodiny;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Skupina", inversedBy="oddily", cascade={"persist"})
     */
    protected $kartoteka;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Cvicitel", mappedBy="oddily")
     */
    protected $cvicitele;

    /**
     * @return mixed
     */
    public function getNazev()
    {
        return $this->nazev;
    }

    /**
     * @param mixed $nazev
     */
    public function setNazev($nazev)
    {
        $this->nazev = $nazev;
    }

    /**
     * @return mixed
     */
    public function getHodiny()
    {
        return $this->hodiny;
    }

    /**
     * @param mixed $hodiny
     */
    public function setHodiny($hodiny)
    {
        $this->hodiny = $hodiny;
    }

    /**
     * @return mixed
     */
    public function getKartoteka()
    {
        return $this->kartoteka;
    }

    /**
     * @param mixed $kartoteka
     */
    public function setKartoteka($kartoteka)
    {
        $this->kartoteka = $kartoteka;
    }

    /**
     * @return mixed
     */
    public function getCvicitele()
    {
        return $this->cvicitele;
    }

    /**
     * @param mixed $cvicitele
     */
    public function setCvicitele($cvicitele)
    {
        $this->cvicitele = $cvicitele;
    }

    /**
     * @ORM\PostLoad()
     */
    public function postLoad()
    {
        /** @var Skupina $skupina */
        $skupina = $this->getKartoteka();
        if ($skupina !== null) {
            $this->nazev = $skupina->getNazev();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $nadrazenaSkupina = $this->getKartoteka();

        $skupinaTyp = $em->getRepository("AppBundle:SkupinaTyp")->find('Oddíl');
        if ($skupinaTyp == null) {
            $skupinaTyp = new SkupinaTyp();
            $skupinaTyp->setId('Oddíl');
        }

        $skupina = new Skupina();
        $skupina->setNazev($this->getNazev());
        $skupina->setSkupinaTyp($skupinaTyp);
        $skupina->setNadrazenaSkupina($nadrazenaSkupina);

        $this->setKartoteka($skupina);
    }

    /**
     * @ORM\PreFlush()
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        $this->getKartoteka()->setNazev($this->getNazev());
    }

    function __toString()
    {
        return (string)$this->getNazev();
    }
}

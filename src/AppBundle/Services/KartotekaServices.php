<?php


namespace AppBundle\Services;


use AppBundle\Entity\Osoba;
use AppBundle\Entity\OsobaSkupina;
use AppBundle\Entity\Skupina;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class KartotekaServices {

    /**
     * @var TokenStorage
     */
    private $tokenStorage;
    /**
     * @var EntityManager
     */
    private $entityManager;


    public function __construct(TokenStorage $tokenStorage, EntityManager $entityManager) {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    public function getKartoteky(){
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        /** @var Osoba $osoba */
        $osoba = $user->getOsoba();

        if($osoba==null) {
            $skupiny = $this->entityManager->getRepository('AppBundle:Skupina')->findAll();
        } else {
            $skupiny = $osoba->getSkupiny();
        }
        
        return array_filter($skupiny, function(Skupina $skupina){
            return $skupina->getSkupinaTyp() == "Kartotéka";
        });

    }
}

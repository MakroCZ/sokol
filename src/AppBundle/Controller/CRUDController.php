<?php


namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CRUDController extends Controller {

    public function showMembersAction() {

        $object = $this->admin->getSubject();

        if(!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        var_dump($object);
    }

}
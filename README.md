Systém pro evidenci členské základny spolku.
Systém vznikl jako maturitní projekt na Obchodní Akademii v Uherském Hradišti a byl vytvořen pro T. J. Sokol Uherské Hradiště.

Systém je založen na frameworku Symfony a balícčích Sonata.

Jedná se open source systém, takže kdokoli může upravovat, měnit nebo jinak využívat zdrojové kódy pro svou potřebu, ale prosím o opětovné zveřejnění.

Použití systému v praxi je na vlastní riziko a autor/autoři nenesou zodpovědnost za případné chyby.